package com.remeeting.myapplication;

public class UXEvent {
    enum Type {
        BTN_CLICKED,
        RADIO_SELECTED
    }

    Type type;
    Object payload;

    public UXEvent(Type type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public String toString() {
        return type.toString() + (payload == null ? "" : ": " + payload.toString());
    }
}
