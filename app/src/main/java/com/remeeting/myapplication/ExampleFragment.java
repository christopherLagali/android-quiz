package com.remeeting.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.ButterKnife;

public class ExampleFragment extends Fragment {
    private TextView externalTextView;

    RadioGroup radioGroup;
    Button button;
    TextView textView;

    void onButtonClick() {
        Bundle args = getArguments();
        args.putString("type", "btn-click");
        BusProvider.getInstance().post(new UXEvent(UXEvent.Type.BTN_CLICKED, args));
        externalTextView.setText("Event originated fragment " + args.getInt("id"));
    }

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);
        externalTextView = ((MainActivity) getActivity()).textView1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_example, container, false);

        ButterKnife.bind(this, v);

        textView.setText("Fragment number " + getArguments().getInt("id"));

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Bundle args = getArguments();
                args.putString("type", "radio-" + checkedId);
                BusProvider.getInstance().post(new UXEvent(UXEvent.Type.RADIO_SELECTED, args));
                externalTextView.setText("Event originated fragment " + args.getInt("id"));
            }
        });

        return v;
    }
}
