package com.remeeting.myapplication;

import com.squareup.otto.Bus;

/**
 * Created by sikoried on 9/21/15.
 */
public class BusProvider {
    public static Bus getInstance() {
        return new Bus();
    }
}
