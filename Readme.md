This is a simple app that has a number of Fragments in a ViewPager, each 
displaying its fragment id, a button and two radio buttons.  After fixing all
the issues, you should observe the following behavior:

- The app should compile and not exhibit any exceptions or crashes.

- Each fragment shows the correct id.

- The main activity shows only events that originate from direct user touch
  inputs.

Note that the app uses two external toolkits (otto and butterknife). It's *not*
part of the solution to remove this dependency or their usage :-)